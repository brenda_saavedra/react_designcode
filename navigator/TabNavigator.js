import React from 'react';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';
import HomeScreen from '../screens/HomeScreen';
import SectionScreen from '../screens/SectionScreen';
import { Ionicons } from '@expo/vector-icons'
import CoursesScreen from '../screens/CoursesScreen';
import ProjectsScreen from '../screens/ProjectsScreen';

const Home = createStackNavigator({
    Home:HomeScreen, 
    Section: SectionScreen
},{
    mode: "modal"
});

Home.navigationOptions = ({ navigation }) => {
    let tabBarVisible = true;
    for (let i = 0; i < navigation.state.routes.length; i++) {
      if (navigation.state.routes[i].routeName == "Section") {
        tabBarVisible = false;
      }
    }
  
    return {
      tabBarVisible
    };
};

const Couses = createStackNavigator({
    Courses: CoursesScreen
});

Couses.navigationOptions = ({ navigation }) => {
    let tabBarVisible = true;
    for (let i = 0; i < navigation.state.routes.length; i++) {
      if (navigation.state.routes[i].routeName == "Section") {
        tabBarVisible = false;
      }
    }
  
    return {
      tabBarVisible
    };
};

const Projects = createStackNavigator({
    Projects: ProjectsScreen
});

Projects.navigationOptions = ({ navigation }) => {
    let tabBarVisible = true;
    for (let i = 0; i < navigation.state.routes.length; i++) {
      if (navigation.state.routes[i].routeName == "Section") {
        tabBarVisible = false;
      }
    }
  
    return {
      tabBarVisible
    };
};

const TabNavigator = createBottomTabNavigator({
    Projects,
    Home,
    Couses
},
{
  defaultNavigationOptions: ({ navigation }) => ({
    
    tabBarIcon: ({ focused, horizontal, tintColor }) => {
      const { routeName } = navigation.state;
      let IconComponent = Ionicons;
      let iconName;

      if (routeName === 'Home') {
        iconName = `ios-home`;
        // Sometimes we want to add badges to some icons.
        // You can check the implementation below.
      } else if (routeName === 'Couses') {
        iconName = `ios-albums`;
      } else {
        iconName = 'ios-folder'
      }
      //console.log("iconName::"+iconName);
      // You can return any component that you like here!
      return <IconComponent name={iconName} size={25} color={tintColor} />;
    },
  }),
  tabBarOptions: {
    activeTintColor: '#4775f2',
    inactiveTintColor: '#b8bece'
  },
});

export default TabNavigator;