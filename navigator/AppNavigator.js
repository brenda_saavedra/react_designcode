import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import HomeScreen from '../screens/HomeScreen.js';
import SectionScreen from '../screens/SectionScreen.js';
import TabNavigator from './TabNavigator.js';

// if you want to have just navigationBar

const AppNavigator = createStackNavigator({
    Home: HomeScreen,
    Section: SectionScreen
},{
    mode:"modal"
})

//export default createAppContainer(AppNavigator);

export default createAppContainer(TabNavigator);