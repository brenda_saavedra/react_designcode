import React from 'react';
import styled from 'styled-components';
import  { connect } from 'react-redux';

function mapStateToProps(state) {
    return {
      name: state.name
    };
}
  
function mapDispatchToProps(dispatch) {
    return {
      updateName: name =>
        dispatch({
          type: "UPDATE_NAME",
          name: name
        })
    };
}
  

class Avatar extends React.Component {

    state = {
        photo:"https://p59.tr1.n0.cdn.getcloudapp.com/items/xQuvlvvp/avatar-default.jpg?v=4aadcdbdffe209bbd523530a75351afd"
    }

    /*
    fetch("https://uifaces.co/api?limit=1&random", {
        headers: new Headers({
            "X-API-KEY": "eeaafbe81657073cd70ac6e3de1bd6"
        })
    })
    
    */


    componentDidMount(){
        fetch("https://uinames.com/api/?ext&region=mexico&gender=female")
          .then(response => response.json())
          .then(response => {
            //console.log(response);

            this.setState({
              photo: response.photo
            });

            this.props.updateName(response.name);
          });

    }

    render(){
        return (
            <Image source={{ uri: this.state.photo }} />
        )
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(Avatar);

const Image = styled.Image`
  width: 44px;
  height: 44px;
  background: black;
  border-radius: 22px;
  top: 0;
  left: 0;
`;